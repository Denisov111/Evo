﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuestLibrary
{
    public enum Sex { Male, Female }

    public class Guest
    {
        Guest() { }

        public Guest(int maxLength) => this.maxLength = maxLength;

        private int maxLength;

        public DateTime? BirthDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public bool IsValid
        {
            get
            {
                if (LastName != null)
                {
                    if (LastName.Length > maxLength)
                        return false;
                }
                if (FirstName != null)
                {
                    if (FirstName.Length > maxLength)
                        return false;
                }
                if (BirthDate == null)
                    return false;
                return true;
            }
        }
    }
}
