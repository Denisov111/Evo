﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Globalization;

namespace EvoTest
{
    public class NameLengthRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (((string)value).Length > Config.maxLength)
            {
                return new ValidationResult(false, "Максимум " + Config.maxLength + " символов");
            }
            return ValidationResult.ValidResult;
        }
    }
}
