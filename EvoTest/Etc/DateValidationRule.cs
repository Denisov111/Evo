﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Globalization;

namespace EvoTest
{
    public class DateValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string error = "Укажите дату";
            if (value != null)
            {
                DateTime dt;
                bool isDt = DateTime.TryParseExact((string)value, "dd.MM.yyyy", null, DateTimeStyles.None, out dt);
                return isDt ? ValidationResult.ValidResult : new ValidationResult(false, error);
            }
            return new ValidationResult(false, error);
        }
    }
}
