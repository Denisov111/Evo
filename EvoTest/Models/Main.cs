﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuestLibrary;

namespace EvoTest
{
    public class Main
    {
        public Guest Guest { get; set; }

        internal void Run()
        {
            Guest = new Guest(Config.maxLength);
            ViewModels.MainViewModel vm = new ViewModels.MainViewModel(Guest);
            MainWindow view = new MainWindow(vm);
            view.Show();
        }
    }
}
