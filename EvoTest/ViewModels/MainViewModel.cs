﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using MahApps.Metro.Controls.Dialogs;
using System.Windows;
using GuestLibrary;
using System.Collections.Concurrent;
using System.Collections;
using System.Windows.Controls;
using System.Globalization;

namespace EvoTest.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public IDialogCoordinator dialogCoordinator;

        public MainViewModel(Guest guest)
        {
            Guest = guest;
        }

        public Guest Guest { get; set; }

        public bool IsMan
        {
            get => Guest.Sex == Sex.Male;
            set => Guest.Sex = value ? Sex.Male : Sex.Female;
        }

        public bool IsWoman
        {
            get => Guest.Sex == Sex.Female;
            set => Guest.Sex = value ? Sex.Female : Sex.Male;
        }

        public string TextBirthDate
        {
            set
            {
                DateTime dt;
                bool isDt = DateTime.TryParseExact((string)value, "dd.MM.yyyy", null, DateTimeStyles.None, out dt);
                if (isDt)
                    Guest.BirthDate = dt;
            }
        }

        public RelayCommand Save { get => new RelayCommand(obj => Show(), canExecute: IsFormValid); }

        public RelayCommand Close { get => new RelayCommand(obj => Application.Current.Shutdown()); }

        private bool IsFormValid(object obj) => Guest.IsValid;

        async private void Show()
        {
            string message = string.Format("Дата рождения: {0}\nИмя: {1}\nФамили и отчество: {2}\nПол: {3}",
                Guest.BirthDate?.ToString("dd-MM-yyyy"), Guest.FirstName, Guest.LastName, Guest.Sex == 0 ? "мужчина" : "женщина");
            await dialogCoordinator.ShowMessageAsync(this, "", message);
            Guest = new Guest(Config.maxLength);
            OnPropertyChanged(string.Empty);
        }

        #region INotifyPropertyChanged code

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        #endregion
    }
}
