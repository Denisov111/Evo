using NUnit.Framework;
using GuestLibrary;
using System;
using System.Text;

namespace GuestTest
{
    public class GuestTests
    {
        int maxLength = 100;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BithdateBadTest()
        {
            Guest guest = new Guest(maxLength);

            bool isValid = guest.IsValid;

            Assert.IsFalse(isValid);
        }

        [Test]
        public void BithdateValidTest()
        {
            Guest guest = new Guest(maxLength);
            guest.BirthDate = DateTime.Now;

            bool isValid = guest.IsValid;

            Assert.IsTrue(isValid);
        }

        [Test]
        public void FirstNameNullTest()
        {
            Guest guest = new Guest(maxLength);
            guest.BirthDate = DateTime.Now;

            bool isValid = guest.IsValid;

            Assert.IsTrue(isValid);
        }

        [Test]
        public void FirstNameValidTest()
        {
            Guest guest = new Guest(maxLength);
            guest.BirthDate = DateTime.Now;
            guest.FirstName = GetNormalString();

            bool isValid = guest.IsValid;

            Assert.IsTrue(isValid);
        }

        [Test]
        public void FirstNameMaxLengthTest()
        {
            Guest guest = new Guest(maxLength);
            guest.BirthDate = DateTime.Now;
            guest.FirstName = GetMaxString();

            bool isValid = guest.IsValid;

            Assert.IsTrue(isValid);
        }

        [Test]
        public void FirstNameVeryLongTest()
        {
            Guest guest = new Guest(maxLength);
            guest.BirthDate = DateTime.Now;
            guest.FirstName = GetLongString();

            bool isValid = guest.IsValid;

            Assert.IsFalse(isValid);
        }

        private string GetLongString() => GetString(1);

        private string GetNormalString() => GetString(-1);

        private string GetMaxString() => GetString(0);

        private string GetString(int difference)
        {
            StringBuilder longString = new StringBuilder();
            while (longString.Length < maxLength + difference)
                longString.Append("a");
            return longString.ToString();
        }
    }
}